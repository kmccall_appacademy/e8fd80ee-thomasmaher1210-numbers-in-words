SINGLES = {
  1 => "one",
  2 => "two",
  3 => "three",
  4 => "four",
  5 => "five",
  6 => "six",
  7 => "seven",
  8 => "eight",
  9 => "nine"
}

TEENS = {
  0 => "ten",
  1 => "eleven",
  2 => "twelve",
  3 => "thirteen",
  4 => "fourteen",
  5 => "fifteen",
  6 => "sixteen",
  7 => "seventeen",
  8 => "eighteen",
  9 => "nineteen"
}

TO_100 = {
  2 => "twenty",
  3 => "thirty",
  4 => "forty",
  5 => "fifty",
  6 => "sixty",
  7 => "seventy",
  8 => "eighty",
  9 => "ninety"
}

class Fixnum

  def array_int
    self.to_s.split("").map! {|d| d.to_i}
  end

  def in_words
    arr = array_int
    words = []

    while arr.length > 0
      pos = arr.length % 3

      if pos == 0 && arr[0..2].reduce(:+) == 0
        arr.shift
        arr.shift
        arr.shift

      elsif pos == 0
        words << hundreds(arr[0])
        arr.shift

      elsif pos == 2
        if arr[0] == 1
          arr.shift
          words << "#{TEENS[arr[0]]}"
          words << magnitude(arr.length)
        else
          words << tens(arr[0])
          if arr[1] == 0
            arr.shift
            words << magnitude(arr.length)
          end
        end
        arr.shift

      else
        if arr[0] == 0
          words << 'zero'
        else
          words << "#{SINGLES[arr[0]]}"
        end
        words << magnitude(arr.length)
        arr.shift
      end
    end
    words.compact.join(" ")
  end

  def hundreds(digit)
    if digit == 0
      nil
    else
      "#{SINGLES[digit]} hundred"
    end
  end

  def tens(digit)
    if digit == 0
      nil
    else
      "#{TO_100[digit]}"
    end
  end

  def magnitude(num)
    if num < 4
      nil
    elsif num < 7
      "thousand"
    elsif num < 10
      "million"
    elsif num < 13
      "billion"
    elsif num >= 13
      "trillion"
    end
  end

end
